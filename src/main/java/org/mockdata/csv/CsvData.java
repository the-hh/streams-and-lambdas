package org.mockdata.csv;

import com.google.common.io.Resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.zip.ZipInputStream;

public class CsvData {
    public record Person(String firstName, String lastName, double salary, String state, char gender) {}

    public static List<Person> importPersons() throws IOException {
            InputStream csvFIleStream = Resources.getResource("Hr5m.zip").openStream();
            ZipInputStream zipInputStream = new ZipInputStream(csvFIleStream); // 0 ja 1 jada
            zipInputStream.getNextEntry();
            InputStreamReader inputStreamReader = new InputStreamReader(zipInputStream); // sümbolite jada
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader); // line data
            return bufferedReader.lines().parallel()
                    .skip(1)
                    .limit(17)
                    .map(CsvData::parse)
                    .toList();
    }

    public static Person parse(String csvLine) {
        String[] dataItem = csvLine.split(",");
        return new Person(dataItem[2], dataItem[4], Double.parseDouble(dataItem[25]), dataItem[32], dataItem[5].strip().charAt(0));
    }
}
