package org.mockdata.json.item;

import java.util.Arrays;

public class CountryByLanguages {

    private final String country;
    private final String[] languages;

    public CountryByLanguages(String country, String[] languages) {
        this.country = country;
        this.languages = languages;
    }

    public String getCountry() {
        return country;
    }

    public String[] getLanguages() {
        return languages;
    }

    @Override
    public String toString() {
        return "countryByLanguages{" +
                "country='" + country + '\'' +
                ", languages=" + Arrays.toString(languages) +
                '}';
    }
}
