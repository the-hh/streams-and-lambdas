package org.mockdata.json.item;

public class CountryByGovernmentType {

    private final String country;
    private final String government;

    public CountryByGovernmentType(String country, String government) {
        this.country = country;
        this.government = government;
    }

    public String getCountry() {
        return country;
    }

    public String getGovernment() {
        return government;
    }

    @Override
    public String toString() {
        return "CountryByGovernmentType{" +
                "country='" + country + '\'' +
                ", government='" + government + '\'' +
                '}';
    }
}
