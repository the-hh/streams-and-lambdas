package org.mockdata.json;

import com.google.common.io.Resources;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

public abstract class JsonData {

    public static<T> List<T> getJsonData(String resourceName, Class<T[]> clazz) throws IOException {
        InputStream inputStream = Resources.getResource(resourceName).openStream();
        String json = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
        T[] arr = new Gson().fromJson(json, clazz);
        return Arrays.asList(arr);
    }

}