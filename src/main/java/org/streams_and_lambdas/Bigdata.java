package org.streams_and_lambdas;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockdata.csv.CsvData;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class Bigdata {
    public static List<CsvData.Person> personList;

    @BeforeAll
    static void beforeAll() throws IOException {
        personList = CsvData.importPersons();
    }

    @Test
    public void testReducePattern() {
        int reduceMin = personList.stream()
                .mapToInt(person -> (int) person.salary())
//                .reduce(1000000, (a, b) -> a < b ? a : b);
//                .reduce(1000000, (a, b) -> Math.min(a, b));
                .reduce(1000000, Math::min);
        System.out.println(reduceMin);
        System.out.println();

        Optional<String> oneString = Stream.of("tom", "jerry", "heh")
                .reduce((a, b) -> a.concat("_").concat(b));

        System.out.println(oneString.orElse("..."));
    }

    @Test
    public void testGroupingRecords() {
        TreeMap<String, List<CsvData.Person>> result1 = personList.stream()
                .collect(Collectors.groupingBy(CsvData.Person::state, TreeMap::new, toList()));
        System.out.println(result1 + "\n");

        TreeMap<String, String> result2 = personList.stream()
//              .collect(groupingBy(CsvData.Person::state, TreeMap::new, summingDouble(CsvData.Person::salary)));
                .collect(groupingBy(CsvData.Person::state, TreeMap::new,
                        collectingAndThen(summingDouble(CsvData.Person::salary), s -> String.format("$%,f.00%n", s))));
        System.out.println(result2 + "\n");

        personList.stream()
                .collect(groupingBy(CsvData.Person::state, TreeMap::new,
                        collectingAndThen(summingDouble(CsvData.Person::salary), NumberFormat.getCurrencyInstance()::format)))
                .forEach((state, salary) -> System.out.printf("%s -> %s%n", state, salary));
    }

    @Test
    public void testNestedGroupings() {
        personList.stream()
                .collect(groupingBy(CsvData.Person::state, TreeMap::new,
                        groupingBy(CsvData.Person::gender, collectingAndThen(
                                summingDouble(CsvData.Person::salary),
                                NumberFormat.getCurrencyInstance()::format))
                ))
                .forEach((state, data) -> System.out.printf("%s -> %s%n", state, data));
        System.out.println();

        personList.stream()
                .collect(
                        groupingBy(CsvData.Person::state, TreeMap::new,
                        groupingBy(CsvData.Person::gender, collectingAndThen(
                                averagingDouble(CsvData.Person::salary),
                                NumberFormat.getCurrencyInstance()::format))
                ))
                .forEach((state, data) -> System.out.printf("%s -> %s%n", state, data));
    }

    @Test
    public void testPartitioning () {
        Map<Boolean, List<CsvData.Person>> genderMap = personList.stream()
                .collect(partitioningBy(p -> p.gender() == 'F'));
        for (Boolean c : genderMap.keySet()) {
            System.out.println(genderMap.get(c));
        }
        System.out.println("---------------------------------------------");

        Map<Boolean, Long> genderCount = personList.stream()
                .collect(partitioningBy(p -> p.gender() == 'F', counting()));
        for (Boolean c : genderCount.keySet()) {
            System.out.println(c + ": " +  genderCount.get(c));
        }
        System.out.println("---------------------------------------------");

        Map<Boolean, Map<String, Long>> booleanMapMap = personList.stream()
                .collect(partitioningBy(p -> p.gender() == 'F',
                        groupingBy(CsvData.Person::state, counting())));

        booleanMapMap.forEach((key, value) -> {
                System.out.println(key + ": ");
                value.entrySet().forEach(e -> System.out.println("\t" + e));
        });
    }

    @Test
    public void testFunctionalMethods () {
        Map<String, Long> result = personList.stream()
                .collect(groupingBy(CsvData.Person::state, counting()));
        System.out.println(result);

        result.replaceAll((k,v) -> v*2);
        System.out.println(result);

        result.replaceAll((k,v) -> k.startsWith("M") ? v * 2 : v );
        System.out.println(result);
    }

}
