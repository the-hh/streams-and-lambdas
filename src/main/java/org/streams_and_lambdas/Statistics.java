package org.streams_and_lambdas;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockdata.csv.CsvData;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.DoubleSummaryStatistics;
import java.util.List;

public class Statistics {

    public static List<CsvData.Person> personList;

    @BeforeAll
    static void beforeAll() throws IOException {
        personList = CsvData.importPersons();
    }

    @Test
    public void getStatistics1()  {
        double statValue = personList.stream()
                .mapToDouble(CsvData.Person::salary)
                .average() // or .min() or .max()
                .orElse(0);
        System.out.println("Statistics1: " + statValue + "\n");
    }

    @Test
    public void getStatistics2()  {
        double statValue = personList.stream()
                .mapToDouble(CsvData.Person::salary)
                .count();  // or .sum()
        System.out.println("Statistics2: " + statValue + "\n");
    }

    @Test
    public void getDoubleSummaryStatistics() {
        DoubleSummaryStatistics statistics = personList.stream()
                .mapToDouble(CsvData.Person::salary)
                .summaryStatistics();
        System.out.println("SummaryStatistics:");
        System.out.println("count: " + statistics.getCount());
        System.out.println("min: " + statistics.getMin());
        System.out.println("max: " + statistics.getMax());
        System.out.println("average: " + statistics.getAverage());
        System.out.println("sum: " + BigDecimal.valueOf(statistics.getSum()));
        System.out.println();
    }
}
