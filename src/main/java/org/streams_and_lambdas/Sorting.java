package org.streams_and_lambdas;

import org.assertj.core.util.Strings;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockdata.json.JsonData;
import org.mockdata.json.item.CountryByGovernmentType;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

public class Sorting {

    public static List<CountryByGovernmentType> countrys;


    @BeforeAll
    static void beforeAll() throws IOException {
        countrys = JsonData.getJsonData("country-by-government-type.json", CountryByGovernmentType[].class);
    }

    @Test
    public void sortingSteamOfElements() {
        List<String> sorted = countrys.stream()
                .map(CountryByGovernmentType::getCountry)
//                .sorted()
                .sorted(Comparator.reverseOrder())
                .toList();
        sorted.forEach(System.out::println);
    }

    @Test
    public void sortingSteamOfObjets() {

        Comparator<CountryByGovernmentType> comparator = Comparator
                .comparing(CountryByGovernmentType::getGovernment).reversed()
                .thenComparing(CountryByGovernmentType::getCountry);

        List<CountryByGovernmentType> sort = countrys.stream()
                .filter(country -> !(Strings.isNullOrEmpty(country.getGovernment())))
                .sorted(comparator)
                .toList();
        sort.forEach(System.out::println);
    }

    @Test
    public void countrysWithoutGovernment() {
        List<CountryByGovernmentType> topTenRepublics = countrys.stream()
                .filter(country -> Strings.isNullOrEmpty(country.getGovernment()))
                .sorted(Comparator.comparing(CountryByGovernmentType::getCountry).reversed())
                .limit(10)  // no effect
                .toList();
        topTenRepublics.forEach(System.out::println);
    }


}
