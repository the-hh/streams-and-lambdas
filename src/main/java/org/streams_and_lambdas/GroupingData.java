package org.streams_and_lambdas;

import org.assertj.core.util.Strings;
import org.junit.jupiter.api.Test;
import org.mockdata.json.JsonData;
import org.mockdata.json.item.CountryByGovernmentType;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GroupingData {

    @Test
    public void simpleGrouping() throws Exception {

        Map<String, List<CountryByGovernmentType>> map = JsonData.getJsonData("country-by-government-type.json", CountryByGovernmentType[].class)
                .stream()
                .filter(country -> !(Strings.isNullOrEmpty(country.getGovernment())))
                .collect(Collectors.groupingBy(CountryByGovernmentType::getGovernment));

        map.forEach((government, countrys) -> {
            System.out.println("Government: " + government);
            countrys.forEach(c -> System.out.println(c.toString()));
            System.out.println("---------------------");
        });

    }


    @Test
    public void groupingAndCounting() {
        List<String> names = List.of(
                "John",
                "John",
                "Mariam",
                "Alex",
                "Mohammado",
                "Mohammado",
                "Vincent",
                "Alex",
                "Alex"
        );

        Map<String, Long> map = names.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(map);

    }
}
