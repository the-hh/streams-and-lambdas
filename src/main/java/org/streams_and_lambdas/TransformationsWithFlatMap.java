package org.streams_and_lambdas;

import org.junit.jupiter.api.Test;
import org.mockdata.json.JsonData;
import org.mockdata.json.item.CountryByLanguages;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class TransformationsWithFlatMap {

    /**
     * flatMap usage
     * distinct and Set usage
     */

    @Test
    public void withFlatMap() throws Exception {

        Set<String> languages = JsonData
                .getJsonData("country-by-languages.json", CountryByLanguages[].class).stream()
                .map(c -> Arrays.stream(c.getLanguages()).toList())
                .flatMap(List::stream)
                .collect(Collectors.toSet());

        System.out.println(languages);
    }

    @Test
    public void flatMapWithOptionals() {
        List<Optional<String>> optionals = List.of(
                Optional.of("Swedish"),
                Optional.empty(),
                Optional.of("Nauru")
        );

        List<String> list = optionals.stream()
                .flatMap(Optional::stream)
                .collect(Collectors.toList());

        System.out.println(list);
    }


}
