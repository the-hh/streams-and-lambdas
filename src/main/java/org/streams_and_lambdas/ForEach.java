package org.streams_and_lambdas;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ForEach {

    @Test
    public void loops1() {
        Integer[] integers = {2, 4, 6, 7, 8, 10, 12, 14, 13, 15};
        Predicate<Integer> even = n -> n % 2 == 0;

        System.out.println("using dropWhile even");
        Stream.of(integers)
                .dropWhile(even)
                .forEach(n -> System.out.print(n + " "));

        System.out.println("\n\n");
        System.out.println("using take while even");
        Stream.of(integers)
                .takeWhile(even)
                .forEach(n -> System.out.print(n + " "));
    }

    @Test
    public void loops2() {
        System.out.println("Print all list items: ");
        IntStream.of(1, 2, 3, 4, 5)
                .forEach(System.out::print);

        System.out.println("\n\n");
        System.out.println("print range 6..12 ");
        IntStream.range(6, 12)
                .forEach(System.out::print);

        System.out.println("\n\n");
        System.out.println("print closed range 6..12 ");
        IntStream.rangeClosed(6, 12)
                .forEach(System.out::print);

        System.out.println("\n\n");
        System.out.println("print concated closed range 1..5 ");
        IntStream.rangeClosed(1, 5)
                .mapToObj(String::valueOf)
                .map(s -> s.concat("-"))
                .forEach(System.out::print);
    }

    @Test
    public void joiningStringsWithStream() {

        System.out.println("print concated closed range 1..5 ");
        IntStream.rangeClosed(1, 5)
                .mapToObj(String::valueOf)
                .map(s -> s.concat("-"))
                .forEach(System.out::print);


        System.out.println("\n\n");
        System.out.println("joining names with | ");
        List<String> names = List.of("anna", "john", "marcos", "helena", "yasmin");
        String join = names.stream()
                .map(name -> name.substring(0, 1).toUpperCase() + name.substring(1))
                .collect(Collectors.joining(" | "));
        System.out.println(join);

    }
}
