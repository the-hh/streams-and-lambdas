package org.streams_and_lambdas;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;

public class Filtering {

    @Test
    public void filtering() {
        Integer[] integers = {2, 4, 6, 7, 8, 10, 12, 14, 13, 15};
        Predicate<Integer> even = n -> n % 2 == 0;
        Predicate<Integer> biggerThan10 = n -> n > 10;

        System.out.println("integers");
        Stream.of(integers)
                .forEach(n -> System.out.print(n + " "));

        System.out.println("\n\n");
        System.out.println("using one filter");
        Stream.of(integers)
                .filter(even)
                .forEach(n -> System.out.print(n + " "));

        System.out.println("\n\n");
        System.out.println("using even and biggerThan10 filter");
        Stream.of(integers)
//                .filter(even)
//                .filter(biggerThan10)
                .filter(even.and(biggerThan10))
                .forEach(n -> System.out.print(n + " "));

        System.out.println("\n\n");
        System.out.println("using not and or filtering");
        Stream.of(integers)
                .filter((not(even)).or(biggerThan10))
                .forEach(n -> System.out.print(n + " "));
    }


    @Test
    public void findFirst() {
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int result = Arrays.stream(numbers).filter(n -> n == 50)
                .findFirst()
                .orElse(-1);
        System.out.println(result);
    }

    @Test
    public void findAny() {
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 10};
        int result = Arrays.stream(numbers)
                .filter(n -> n == 9)
                .findAny()
                .orElse(-1);
        System.out.println(result);
    }

    @Test
    public void allMatch() {
        int[] even = {2, 4, 6, 8, 10};
        boolean allMatch = Arrays.stream(even).allMatch(n -> n % 2 == 0);
        System.out.println(allMatch);
    }

    @Test
    public void anyMatch() {
        int[] evenAndOneOdd = {2, 4, 6, 8, 10, 11};
        boolean anyMatch = Arrays.stream(evenAndOneOdd).anyMatch(n -> !(n % 2 == 0));
        System.out.println(anyMatch);
    }

}
